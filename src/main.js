import Chart from "chart.js/auto";

let ws = new WebSocket("ws://localhost:9000");

ws.addEventListener("open", function () {
  console.log("connessione aperta");
});
ws.addEventListener("message", function (e) {
  let clearData = JSON.parse(e.data);
  let d = console.log(clearData);
});
ws.addEventListener("error", function (error) {
  console.log("errore: " + error);
});
ws.addEventListener("close", function () {
  console.log("connessione chiusa");
});

function initChart() {
  return new Chart(ctx, {
    type: "line",
    data: {
      labels,
      datasets: [
        {
          label: "my four Dataset",
          data,
          fill: false,
          borderColor: "rgb(75, 192, 192)",
          tension: 0.1,
        },
      ],
    },
    options: {
      animation: false,
      scales: {
        x: {
          beginAtZero: true,
          max: 100,
        },
        y: {
          beginAtZero: true,
          max: 100,
        },
      },
    },
  });
}

const ctx = document.getElementById("chart").getContext("2d");

let chart = initChart();
let data = [];
let labels = [];
